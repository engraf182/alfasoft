<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Administrator Alfasoft",
            "email" => "admin@alfasoft.pt",
            "password" => bcrypt("secret")
        ]);
    }
}
