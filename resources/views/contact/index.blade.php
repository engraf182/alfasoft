@extends('layouts.alfasoft')
@section('content')
	<div class="box-border mt-6 ml-2 mr-2 mb-2 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
		<div class="flex justify-between mb-2">
			<p class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">Contacts</p>
			<a href="{{ route('contact.create') }}" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center mr-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
				<svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 mr-2 -ml-1" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
					<path stroke-linecap="round" stroke-linejoin="round" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
				</svg>
				Contact
			</a>
		</div>
		<table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 mb-4">
			<thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
				<tr>
					<th scope="col" class="px-6 py-3">#</th>
					<th scope="col" class="px-6 py-3">Name</th>
					<th scope="col" class="px-6 py-3"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($contacts as $key => $contact)
					<tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50">
						<td scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">{{ $key+1 }}</td>
						<td scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">{{ $contact->name }}</td>
						<td scope="row" class="px-6 py-4 flex flex-row-reverse">
							<div class="inline-flex rounded-md shadow-sm" role="group">
								<a href="{{ route('contact.edit', $contact->id) }}" class="py-2 px-4 text-sm font-medium text-gray-900 bg-white rounded-l-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:bg-gray-700 dark:border-gray-600 dark:text-white dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-blue-500 dark:focus:text-white">
									Edit
								</a>
								<a href="{{ route('contact.show', $contact->id) }}" class="py-2 px-4 text-sm font-medium text-gray-900 bg-white border-t border-b border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:bg-gray-700 dark:border-gray-600 dark:text-white dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-blue-500 dark:focus:text-white">
									Show
								</a>
                                <form id="delete-contact-{{ $contact->id }}" action="{{ route('contact.destroy', $contact->id) }}" method="POST" style="display: none;">
                                    @method('delete')
                                   @csrf
                               </form>
								<a
                                    href="{{ route('contact.destroy', $contact->id) }}"
                                    class="py-2 px-4 text-sm font-medium text-gray-900 bg-white rounded-r-md border border-gray-200 hover:bg-gray-100 hover:text-red-600 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:bg-gray-700 dark:border-gray-600 dark:text-white dark:hover:text-white dark:hover:bg-red-600 dark:focus:ring-blue-500 dark:focus:text-white"
                                    href="{{ route('contact.destroy', $contact->id) }}"
                                    onclick="event.preventDefault(); document.getElementById('delete-contact-{{ $contact->id }}').submit();"
                                    >
									Remove
								</a>
							</div>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
        {{ $contacts->links() }}
	</div>
@endsection
