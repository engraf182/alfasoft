<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::orderBy('name')->paginate(10);
        return view('contact.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required|min:5',
            'contact' => 'required|min:9|max:9',
            'email' => 'required|unique:contacts|max:255|email:rfc,dns'
        ]);

        Contact::create($validator);
        return redirect()->route('contact.index')->with('success', 'Contact created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        return view('contact.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        return view('contact.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  int  $id
     * @return \App\Http\Requests\Request
     */
    public function update(Request $request, Contact $contact)
    {
        $validator = $request->validate([
            'name' => 'min:5',
            'contact' => 'min:9|max:9',
            'email' => 'required|max:255|email:rfc,dns|unique:contacts,email,'.$contact->id
        ]);
        $contact->update($validator);
        return redirect()->route('contact.index')->with('success', 'Contact updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contactName = $contact->name;
        $contact->delete();
        return redirect()->route('contact.index')->with('success', 'The contact <strong>'.$contactName.'</strong> has removed successfully');
    }
}
