<?php

namespace Tests\Unit;

use App\Models\Contact;
use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase
{
    public function test_contact_index_screen_can_be_rendered()
    {
        $response = $this->get('/contact');

        $response->assertStatus(200);
    }

}
