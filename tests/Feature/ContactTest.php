<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Contact;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactTest extends TestCase
{
    use RefreshDatabase;

    public function test_contact_index_screen_can_be_rendered()
    {
        Contact::factory()->create();
        $response = $this->get('/contact');

        $response->assertStatus(200);
    }

    public function test_create_route()
    {
        $user = User::factory()->create();

        $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Gregory Engraf',
                'contact' => '123456789',
                'email' => 'gregory0409@gmail.com'
            ]);

        $this->assertDatabaseHas('contacts', [
            'name' => 'Gregory Engraf',
            'contact' => '123456789',
            'email' => 'gregory0409@gmail.com'
        ]);
    }

    public function test_validade_create_payload()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->post(route('contact.store'), [
            'name' => 'Gr',
            'contact' => '12345678999',
            'email' => 'gregory0409@gmail.com'
        ]);
        $response->assertSessionHasErrors(['name', 'contact']);
    }

    public function test_update_contact()
    {
        $user = User::factory()->create();
        $contact = Contact::factory()->create(['email' => 'gregory04049@gmail.com']);

        $this->actingAs($user);

        $this->put(route('contact.update', $contact), [
            'email' => 'greg@pixel3.solutions'
        ]);

        $this->assertDatabaseHas('contacts', [
            'id'  => $contact->id,
            'email' => 'greg@pixel3.solutions'
        ]);
    }
}
